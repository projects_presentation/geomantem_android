package com.somethingapps.geoassistance;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;
import com.somethingapps.geoassistance.RequestManagerDataParser.Route;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class StartRoutesListAdapter extends BaseAdapter implements View.OnClickListener, ServerRequest.IServerRequest {
    private Context context;
    private ArrayList<Route> routes;
    private Route selectedRoute = null;
    private static LayoutInflater inflater = null;

    public StartRoutesListAdapter(Context context, ArrayList<Route> routes) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.routes = routes;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return routes.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return routes.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.new_route_list_row, null);
        }

        TextView typeLocation = (TextView) vi.findViewById(R.id.type_location_row_text);
        TextView subTypeDescription = (TextView) vi.findViewById(R.id.sub_type_description_row_text);
        TextView state = (TextView) vi.findViewById(R.id.state_row_text);
        TextView serialNumber = (TextView) vi.findViewById(R.id.serial_number_row_text);
        TextView routeData = (TextView) vi.findViewById(R.id.route_data_row_text);

        String typeLocationText = routes.get(position).getLocation();
        String subTypeDescriptionText = routes.get(position).getSubTypeDescription();

        Date started = Utilities.convertDate(routes.get(position).getStarted());
        String stateText;

        Button startRouteBtn = (Button) vi.findViewById(R.id.start_route_row_btn);
        startRouteBtn.setTag(routes.get(position));
        startRouteBtn.setOnClickListener(this);

        if (started == null) {
            stateText = vi.getResources().getString(R.string.not_started);

        } else {
            stateText = vi.getResources().getString(R.string.started) + ": " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(started);
            startRouteBtn.setVisibility(View.GONE);
        }

        String serialNumberText = vi.getResources().getString(R.string.serial_number) + ": " + routes.get(position).getSerialNumber();
        int seconds = routes.get(position).getEstimatedDuration();
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        String time = String.format("%02dh:%02dm:%02ds", hours, minutes, seconds);
        String distance = new DecimalFormat("#.000").format(routes.get(position).getEstimatedDistance() * 0.001) + " Kms";
        String routeDataText = vi.getResources().getString(R.string.estimation) + " " + time + " " + distance;

        typeLocation.setText(typeLocationText);
        subTypeDescription.setText(subTypeDescriptionText);
        state.setText(stateText);
        serialNumber.setText(serialNumberText);
        routeData.setText(routeDataText);

        return vi;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.start_route_row_btn:
                selectedRoute = (Route) view.getTag();
                ShowYesNoDialog();
                break;
        }
    }

    private void ShowYesNoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(context.getResources().getString(R.string.start_new_route));
        builder.setMessage(context.getResources().getString(R.string.start_route_are_you_sure));

        builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                UpdateAndStartRoute();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            switch (serverRequestResponse.getServerRequestType().getRequestType()) {
                case UPDATE_ROUTE_STARTED:
                    onUpdateRouteDataRequestResponse(serverRequestResponse);
                    break;
            }
        } catch (Exception ex) {
            Toast.makeText(context, context.getResources().getString(R.string.unknown_error) + "\n" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void UpdateAndStartRoute() {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ServerRequest serverRequest = new ServerRequest(context);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.UPDATE_ROUTE_STARTED, context);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("route_id", Integer.toString(selectedRoute.getId())));
        serverRequestData.AddQueryString(new QueryData("started", format.format(new Date())));
        serverRequest.callRequest(serverRequestData);

    }

    public void onUpdateRouteDataRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            JSONObject jsonObject = new JSONObject(serverRequestResponse.getResult().toString());
            boolean result = jsonObject.getBoolean("updateResult");
            if (result) {
                Intent myIntent = new Intent(context, RoutePointsActivity.class);
                myIntent.putExtra("routeId", selectedRoute.getId());
                context.startActivity(myIntent);
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.error_updating_route), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(context, context.getResources().getString(R.string.error_updating_route) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
