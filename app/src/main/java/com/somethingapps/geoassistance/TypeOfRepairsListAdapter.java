package com.somethingapps.geoassistance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;


import com.somethingapps.geoassistance.RequestManagerDataParser.TypeOfRepair;

import java.util.ArrayList;

public class TypeOfRepairsListAdapter extends BaseAdapter implements CheckBox.OnClickListener {

    private ArrayList<TypeOfRepair> typeOfRepairsList = new ArrayList<>();
    private static LayoutInflater inflater = null;

    public TypeOfRepairsListAdapter(Context context, ArrayList<TypeOfRepair> typeOfRepairsList) {
        this.typeOfRepairsList = typeOfRepairsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return typeOfRepairsList.size();
    }

    @Override
    public Object getItem(int i) {
        return typeOfRepairsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return typeOfRepairsList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null) {
            vi = inflater.inflate(R.layout.repair_type_list_row, null);
        }

        CheckBox checkBox = (CheckBox) vi.findViewById(R.id.repair_type_check_box);
        checkBox.setOnClickListener(this);
        checkBox.setTag(i);
        checkBox.setText(typeOfRepairsList.get(i).getDescription());
        checkBox.setChecked(typeOfRepairsList.get(i).isSelected());

        return vi;
    }

    @Override
    public void onClick(View view) {

        CheckBox checkBox = (CheckBox) view;
        checkBox.setChecked(checkBox.isChecked());
        typeOfRepairsList.get((int)checkBox.getTag()).setSelected(checkBox.isChecked());
    }
}
