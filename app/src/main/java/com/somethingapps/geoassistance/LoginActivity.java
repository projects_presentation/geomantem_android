package com.somethingapps.geoassistance;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;

public class LoginActivity extends Activity implements View.OnClickListener, ServerRequest.IServerRequest {



    private Button loginBtn;
    private EditText email,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        keepSessionId();
        super.onCreate(savedInstanceState);
        if(LoggedInUser.isLoggedIn(this)){

            Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
            LoginActivity.this.startActivity(myIntent);
            finish();
        }
        else {
            startUI();
        }
    }

    private void keepSessionId(){
        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);
    }

    private void startUI(){
        setContentView(R.layout.login_activity);

        loginBtn = (Button) findViewById(R.id.loginBtn);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        loginBtn.setOnClickListener(this);
        setUIFonts();
    }

    private void setUIFonts(){
        TextView loginAppName = (TextView)findViewById(R.id.loginAppName);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/main.ttf");
        loginAppName.setTypeface(custom_font);

        EditText password = (EditText) findViewById(R.id.password);
        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loginBtn:
                ValidateLogin();
                break;
        }
    }

    private void ValidateLogin(){
        if(!Utilities.isValidEmail(email.getText().toString())){
            Toast.makeText(this,R.string.invalid_email_format,Toast.LENGTH_SHORT).show();
            return;
        }
        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.LOGIN,this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("email", email.getText().toString()));
        serverRequestData.AddQueryString(new QueryData("password",password.getText().toString()));
        serverRequest.callRequest(serverRequestData);
    }

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            JSONObject jsonObject = new JSONObject(serverRequestResponse.getResult().toString());
            if(LoggedInUser.login(this,jsonObject)){
                Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
                LoginActivity.this.startActivity(myIntent);
            }
            else {
                Toast.makeText(this, getResources().getString(R.string.invalid_login_data), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, getResources().getString(R.string.unknown_error) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
