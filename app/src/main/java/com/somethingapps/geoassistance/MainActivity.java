package com.somethingapps.geoassistance;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ServerRequest.IServerRequest, StartRouteFragment.OnFragmentInteractionListener, AccountSettingsFragment.OnFragmentInteractionListener, AddEquipmentActivity.OnFragmentInteractionListener{

    private TextView ndUserName, ndUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startUI();

    }

    private void startUI(){

        setContentView(R.layout.main_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Utilities.requestPermissions(this);

   }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        ndUserName = (TextView) findViewById(R.id.drawerUsernameTv);
        ndUserEmail = (TextView) findViewById(R.id.drawerEmailTv);

        ndUserName.setText(LoggedInUser.getUserName(this));
        ndUserEmail.setText(LoggedInUser.getUserEmail(this));

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.start_route_nd_btn:
                fragment = new StartRouteFragment();
                break;
            case R.id.add_new_equipment_nd_btn:
                Intent myIntent = new Intent(MainActivity.this, AddEquipmentActivity.class);
                MainActivity.this.startActivity(myIntent);
                break;
            case R.id.account_settings_nd_btn:
                fragment = new AccountSettingsFragment();
                break;
            case R.id.logout_nd_btn:
                logout();
                return false;
        }
        if(fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainActivityContent, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void logout(){

        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.LOGIN,this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequest.callRequest(serverRequestData);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(this, "Fragment started!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            JSONObject jsonObject = new JSONObject(serverRequestResponse.getResult().toString());
            Boolean logoutResult = jsonObject.getBoolean("logoutResult");
            if(logoutResult == true){
                LoggedInUser.logout(this);
                Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
                MainActivity.this.startActivity(myIntent);
                finish();
            }
            else {
                Toast.makeText(this, getResources().getString(R.string.error_logging_out), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, getResources().getString(R.string.unknown_error) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
