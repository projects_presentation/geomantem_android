package com.somethingapps.geoassistance;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jose on 14/05/2017.
 */

public class Utilities {

    private static int PERMISSION_ALL = 1;

    private static final String[] permissions = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    private static boolean hasPermissions(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void requestPermissions(Context context) {
        if (!hasPermissions(context)) {
            ActivityCompat.requestPermissions((Activity) context, permissions, PERMISSION_ALL);
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        return !(target == null) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private final static String DEBUG_TAG = "MY-SHIFTS-DEBUG";

    public final static void logToConsole(String message) {
        Log.d(DEBUG_TAG, message);
    }

    public final static void logToConsole(Exception exception) {
        if (exception.getMessage() != null) {
            Log.d(DEBUG_TAG, exception.getMessage());
        }
    }

    public static void logMessage(String message) {
        Log.d("PVMDebug", message);
    }

    public static Date convertDate(String dateString) {
        Date parsed = new Date();
        try {
            //2017-06-19T15:55:55.000Z
            SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            parsed = format.parse(dateString);
        } catch (ParseException pe) {
            return null;
        }
        return parsed;
    }
}
