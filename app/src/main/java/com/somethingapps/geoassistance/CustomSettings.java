package com.somethingapps.geoassistance;

/**
 * Created by jose on 25/05/2017.
 */

public class CustomSettings {

    private final static String SERVER_ADDRESS_DEBUG = "http://192.168.8.103";
    private final static String SERVER_ADDRESS_RELEASE = "http://geoassistance.ddns.net";
    private final static int SERVER_PORT_DEBUG = 3000;
    private final static int SERVER_PORT_RELEASE = 3000;

    public static String getServerAddress(){
        String serverAddress;
        if(BuildConfig.DEBUG){
            serverAddress = SERVER_ADDRESS_DEBUG;
        }
        else {
            serverAddress = SERVER_ADDRESS_RELEASE;
        }
        return serverAddress;
    }

    public static int getServerPort(){
        int serverPort = 0;
        if(BuildConfig.DEBUG){
            serverPort = SERVER_PORT_DEBUG;
        }
        else {
            serverPort = SERVER_PORT_RELEASE;
        }
        return serverPort;
    }

}
