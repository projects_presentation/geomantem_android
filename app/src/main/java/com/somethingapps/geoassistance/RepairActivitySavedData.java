package com.somethingapps.geoassistance;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RepairActivitySavedData {


    private static Map<Integer, RepairActivityData> repairActivityDataList = new HashMap<>();

    public static void addActivityData(int routePointId, Bitmap thumbnail, Bitmap image){
        if(repairActivityDataList.containsKey(routePointId)){
            RepairActivityData dummy = repairActivityDataList.get(routePointId);
            dummy.addImage(thumbnail, image);
        }
        else {
            RepairActivityData repairActivityData = new RepairActivityData();
            repairActivityData.addImage(thumbnail, image);
            repairActivityDataList.put(routePointId, repairActivityData);
        }
    }

    public static void addObservations(int routePointId, String observations){
        if(repairActivityDataList.containsKey(routePointId)){
            RepairActivityData dummy = repairActivityDataList.get(routePointId);
            dummy.setObservations(observations);
        }
        else {
            RepairActivityData repairActivityData = new RepairActivityData();
            repairActivityData.setObservations(observations);
            repairActivityDataList.put(routePointId, repairActivityData);
        }
    }

    public static RepairActivityData getRepairActivityData(int routePointId){
        return repairActivityDataList.get(routePointId);
    }

    public static void removeAllById(int routePointId){
        if(repairActivityDataList.containsKey(routePointId)){
            repairActivityDataList.remove(routePointId);
        }
    }
}
