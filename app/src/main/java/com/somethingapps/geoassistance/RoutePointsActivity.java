package com.somethingapps.geoassistance;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;
import com.somethingapps.geoassistance.RequestManagerDataParser.RoutePoint;
import com.somethingapps.geoassistance.RequestManagerDataParser.RoutePointsRequestResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RoutePointsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, ServerRequest.IServerRequest {

    private ListView listview;
    private Button endRouteBtn, addEquipmentToRouteBtn;
    private int routeId = 0;
    private int typeId = 0;
    private int subTypeId = 0;

    Location location = null;
    LocationManager locationManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        routeId = getIntent().getExtras().getInt("routeId");
        typeId = getIntent().getExtras().getInt("typeId");
        subTypeId = getIntent().getExtras().getInt("subTypeId");
        startUI();
        GetAllRoutePoints();
    }

    private void startUI() {
        setContentView(R.layout.route_points_activity);
        endRouteBtn = (Button) findViewById(R.id.end_route_btn);
        addEquipmentToRouteBtn = (Button) findViewById(R.id.add_equipment_to_route_btn);
        endRouteBtn.setOnClickListener(this);
        addEquipmentToRouteBtn.setOnClickListener(this);
        listview = (ListView) findViewById(R.id.route_points_list);
        listview.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        listview.setAdapter(null);
        GetAllRoutePoints();
    }

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            switch (serverRequestResponse.getServerRequestType().getRequestType()) {
                case UPDATE_ROUTE_ENDED:
                    onUpdateRouteDataRequestResponse(serverRequestResponse);
                    break;
                case GET_ROUTE_POINTS:
                    onGetAllRoutePointsResult(serverRequestResponse);
                    break;
            }
        } catch (Exception ex) {
            Toast.makeText(this, getResources().getString(R.string.unknown_error) + "\n" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void GetAllRoutePoints() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {
            location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        } catch (SecurityException e) {
            Toast.makeText(this, "Error getting GPS location!", Toast.LENGTH_SHORT).show();
        }

        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.GET_ROUTE_POINTS, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("route_id", Integer.toString(routeId)));
        serverRequestData.AddQueryString(new QueryData("latitude", Double.toString(location.getLatitude())));
        serverRequestData.AddQueryString(new QueryData("longitude", Double.toString(location.getLongitude())));
        serverRequest.callRequest(serverRequestData);

    }

    private void onGetAllRoutePointsResult(ServerRequestResponse serverRequestResponse) {
        try {
            JSONArray jsonArray = new JSONArray(serverRequestResponse.getResult().toString());
            RoutePointsRequestResult routePointsRequestResult = new RoutePointsRequestResult(jsonArray);
            listview.setAdapter(new RoutePointsListAdapter(this, routePointsRequestResult.getRoutePointsList()));
        } catch (Exception e) {
            Toast.makeText(this, getResources().getString(R.string.error_getting_routes) + "\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.end_route_btn:
                ShowYesNoDialog();
                break;
            case R.id.add_equipment_to_route_btn:
                Intent myIntent = new Intent(RoutePointsActivity.this, AddEquipmentActivity.class);
                myIntent.putExtra("routeId", routeId);
                myIntent.putExtra("typeId", typeId);
                myIntent.putExtra("subTypeId", subTypeId);

                RoutePointsActivity.this.startActivity(myIntent);
                break;
        }
    }

    private void ShowYesNoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getResources().getString(R.string.end_route));
        builder.setMessage(getResources().getString(R.string.end_route_are_you_sure));

        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                markRouteAsTerminated();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void markRouteAsTerminated() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.UPDATE_ROUTE_ENDED, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("route_id", Integer.toString(routeId)));
        serverRequestData.AddQueryString(new QueryData("ended", format.format(new Date())));
        serverRequest.callRequest(serverRequestData);
    }

    private void onUpdateRouteDataRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            JSONObject jsonObject = new JSONObject(serverRequestResponse.getResult().toString());
            Boolean result = jsonObject.getBoolean("updateResult");
            if (result) {
                Intent myIntent = new Intent(RoutePointsActivity.this, MainActivity.class);
                startActivity(myIntent);
            } else {
                Toast.makeText(this, getResources().getString(R.string.error_updating_route), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, getResources().getString(R.string.error_updating_route) + "\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        RoutePoint routePoint = (RoutePoint) adapterView.getItemAtPosition(i);
        Intent myIntent = new Intent(this, RepairActivity.class);
        myIntent.putExtra("id", routePoint.getId());
        myIntent.putExtra("geoElementId", routePoint.getGeoElementId());
        if (Utilities.convertDate(routePoint.getStarted()) != null) {
            startActivity(myIntent);
        }

    }

}
