package com.somethingapps.geoassistance;

import android.graphics.Bitmap;

import com.somethingapps.geoassistance.RequestManagerDataParser.TypeOfRepair;

import java.util.ArrayList;

public class RepairActivityData {
    private String observations = "";
    private ArrayList<TypeOfRepair> typeOfRepairs = new ArrayList<>();
    private ArrayList<Bitmap> thumbnails = new ArrayList<>();
    private ArrayList<Bitmap> images = new ArrayList<>();

    public RepairActivityData(){
        // No ops constructor
    }

    public void addTypeOfRepair(TypeOfRepair typeOfRepair){
        typeOfRepairs.add(typeOfRepair);
    }

    public void addImage(Bitmap thumbnail, Bitmap image){
        thumbnails.add(thumbnail);
        images.add(image);
    }

    public void setObservations(String observations){
        this.observations = observations;
    }

    public String getObservations() {
        return observations;
    }

    public ArrayList<TypeOfRepair> getTypeOfRepairs() {
        return typeOfRepairs;
    }

    public ArrayList<Bitmap> getThumbnails() {
        return thumbnails;
    }

    public ArrayList<Bitmap> getImages() {
        return images;
    }
}
