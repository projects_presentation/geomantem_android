package com.somethingapps.geoassistance;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;
import com.somethingapps.geoassistance.RequestManagerDataParser.EquipmentLocation;
import com.somethingapps.geoassistance.RequestManagerDataParser.EquipmentLocationRequestResults;
import com.somethingapps.geoassistance.RequestManagerDataParser.EquipmentType;
import com.somethingapps.geoassistance.RequestManagerDataParser.EquipmentTypeRequestResults;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class AddEquipmentActivity extends AppCompatActivity implements View.OnClickListener, Spinner.OnItemSelectedListener, OnMapReadyCallback, GoogleMap.OnCameraMoveListener, ServerRequest.IServerRequest {

    private OnFragmentInteractionListener mListener;

    private Spinner equipmentLocationSpinner;
    private Spinner equipmentTypeSpinner;
    private EditText equipmentNumberEditText;
    private EditText equipmentObservationsEditText;
    private Button addEquipmentButton;

    private GoogleMap googleMap;
    private MapView mapView;
    private Marker selectedLocation;

    private int routeId = 0;
    private int typeId = 0;
    private int subTypeId = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getExtras() != null) {
            routeId = getIntent().getExtras().getInt("routeId");
            typeId = getIntent().getExtras().getInt("typeId");
            subTypeId = getIntent().getExtras().getInt("subTypeId");
        }
        startUI();
        setupMap(savedInstanceState);
    }


    private void startUI() {

        setContentView(R.layout.add_equipment_activity);

        equipmentLocationSpinner = (Spinner) findViewById(R.id.new_equipment_location_spinner);
        equipmentLocationSpinner.setOnItemSelectedListener(this);
        equipmentTypeSpinner = (Spinner) findViewById(R.id.new_equipment_type_spinner);
        equipmentNumberEditText = (EditText) findViewById(R.id.equipment_number_ET);
        equipmentObservationsEditText = (EditText) findViewById(R.id.equipment_observations_ET);
        addEquipmentButton = (Button) findViewById(R.id.add_equipment_btn);
        addEquipmentButton.setOnClickListener(this);

        fillLocationsSpinner();


    }

    //region Location Spinner
    private void fillLocationsSpinner() {
        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.GET_EQUIPMENTS_LOCATIONS, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("web_user_id", LoggedInUser.getWebUserId(this)));
        serverRequest.callRequest(serverRequestData);
    }

    private void UpdateLocationsSpinner(ServerRequestResponse serverRequestResponse) {
        try {
            JSONArray jsonArray = new JSONArray(serverRequestResponse.getResult().toString());
            EquipmentLocationRequestResults result = new EquipmentLocationRequestResults(jsonArray);
            ArrayAdapter listOfLocations = new ArrayAdapter(this, android.R.layout.simple_spinner_item, result.getEquipmentLocationsList());
            equipmentLocationSpinner.setAdapter(listOfLocations);
            int index = 0;
            if (routeId != 0) {
                equipmentLocationSpinner.setEnabled(false);
                for (EquipmentLocation equipmentLocation : result.getEquipmentLocationsList()
                        ) {
                    if (equipmentLocation.getId() == typeId) {
                        equipmentLocationSpinner.setSelection(index);
                        break;
                    }
                    ++index;
                }
            }
        } catch (JSONException e) {
            Toast.makeText(this, getResources().getString(R.string.error_getting_equipment_locations) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    //endregion

    //region Equipment types spinner
    private void fillEquipmentTypesSpinner(int typeId) {
        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.GET_EQUIPMENT_TYPES, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("type_id", Integer.toString(typeId)));
        serverRequest.callRequest(serverRequestData);
    }

    private void UpdateEquipmentTypesSpinner(ServerRequestResponse serverRequestResponse) {
        try {
            JSONArray jsonArray = new JSONArray(serverRequestResponse.getResult().toString());
            EquipmentTypeRequestResults result = new EquipmentTypeRequestResults(jsonArray);
            ArrayAdapter listOfLocations = new ArrayAdapter(this, android.R.layout.simple_spinner_item, result.getEquipmentTypesList());
            equipmentTypeSpinner.setAdapter(listOfLocations);
            int index = 0;
            if (routeId != 0) {
                equipmentTypeSpinner.setEnabled(false);
                for (EquipmentType equipmentType : result.getEquipmentTypesList()
                        ) {
                    if (equipmentType.getId() == subTypeId) {
                        equipmentTypeSpinner.setSelection(index);
                        break;
                    }
                    ++index;
                }
            }
        } catch (JSONException e) {
            Toast.makeText(this, getResources().getString(R.string.error_getting_equipment_types) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    //endregion

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.new_equipment_location_spinner:
                EquipmentLocation equipmentLocation = (EquipmentLocation) adapterView.getItemAtPosition(i);
                fillEquipmentTypesSpinner(equipmentLocation.getId());
                break;
            case R.id.new_equipment_type_spinner:
                EquipmentType equipmentType = (EquipmentType) adapterView.getItemAtPosition(i);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void setupMap(Bundle savedInstanceState) {
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        MapsInitializer.initialize(this);
        mapView.getMapAsync(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_equipment_btn:
                InsertEquipment();
                break;
        }
    }

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            switch (serverRequestResponse.getServerRequestType().getRequestType()) {
                case GET_EQUIPMENTS_LOCATIONS:
                    UpdateLocationsSpinner(serverRequestResponse);
                    break;
                case GET_EQUIPMENT_TYPES:
                    UpdateEquipmentTypesSpinner(serverRequestResponse);
                    break;
                case INSERT_EQUIPMENT:
                    onInsertEquipmentRequestResponse(serverRequestResponse);
                    break;
                case INSERT_EQUIPMENT_IN_ROUTE:
                    onInsertEquipmentIntoRouteRequestResponse(serverRequestResponse);
                    break;
            }
        } catch (Exception ex) {
            Toast.makeText(this, getResources().getString(R.string.unknown_error) + "\n" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.setOnCameraMoveListener(this);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        try {
            googleMap.setMyLocationEnabled(true);
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        LatLng center = new LatLng(39.488138, -8.209881);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(center));
    }

    @Override
    public void onCameraMove() {
        googleMap.clear();
        selectedLocation = googleMap.addMarker(new MarkerOptions()
                .position(googleMap.getCameraPosition().target)
        );
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void InsertEquipment() {
        String number = equipmentNumberEditText.getText().toString();
        String observations = equipmentObservationsEditText.getText().toString();

        if (number.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.equipment_number_validation_error), Toast.LENGTH_SHORT).show();
            return;
        }

        if (selectedLocation == null) {
            Toast.makeText(this, getResources().getString(R.string.select_equipment_location), Toast.LENGTH_SHORT).show();
            return;
        }

        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.INSERT_EQUIPMENT, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("description", number));
        serverRequestData.AddQueryString(new QueryData("observations", observations));
        serverRequestData.AddQueryString(new QueryData("lat", Double.toString(selectedLocation.getPosition().latitude)));
        serverRequestData.AddQueryString(new QueryData("long", Double.toString(selectedLocation.getPosition().longitude)));
        serverRequestData.AddQueryString(new QueryData("sub_type_id", Integer.toString(((EquipmentType) equipmentTypeSpinner.getSelectedItem()).getId())));
        serverRequestData.AddQueryString(new QueryData("repair_type_id", "4"));
        serverRequest.callRequest(serverRequestData);
    }

    private void onInsertEquipmentRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            JSONObject jsonObject = new JSONObject(serverRequestResponse.getResult().toString());
            int insertId = jsonObject.getInt("insertId");
            if (insertId != 0) {
                Toast.makeText(this, getResources().getString(R.string.equipment_inserted_successfully), Toast.LENGTH_SHORT).show();
                if (routeId != 0) {
                    InsertPointIntoRoute(insertId);
                } else {
                    finish();
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.error_inserting_equipment), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            Toast.makeText(this, getResources().getString(R.string.error_inserting_equipment) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void InsertPointIntoRoute(int geoElementId) {

        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.INSERT_EQUIPMENT_IN_ROUTE, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("route_id", Integer.toString(routeId)));
        serverRequestData.AddQueryString(new QueryData("geo_element_id", Integer.toString(geoElementId)));
        serverRequest.callRequest(serverRequestData);

    }

    private void onInsertEquipmentIntoRouteRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            JSONObject jsonObject = new JSONObject(serverRequestResponse.getResult().toString());
            boolean result = jsonObject.getBoolean("insertResult");
            if (result) {
                Toast.makeText(this, getResources().getString(R.string.add_equipment_to_route_success), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(this, getResources().getString(R.string.add_equipment_to_route_unsuccess), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            Toast.makeText(this, getResources().getString(R.string.error_inserting_equipment) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

}
