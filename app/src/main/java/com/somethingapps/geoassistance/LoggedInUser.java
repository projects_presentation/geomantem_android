package com.somethingapps.geoassistance;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ProviderInfo;

import org.json.JSONException;
import org.json.JSONObject;

public class LoggedInUser {

    public static String getUserId(Context context) {
        return GetSharedPreference(context,"user_id");
    }

    public static String getWebUserId(Context context){
        return GetSharedPreference(context,"web_user_id");
    }

    public static String getUserName(Context context) {
        return GetSharedPreference(context,"user_name");
    }

    public static String getUserEmail(Context context) {
        return GetSharedPreference(context,"user_email");
    }

    private static String GetSharedPreference(Context context, String preferenceToGet){
        SharedPreferences pref = context.getSharedPreferences("GeoAssistancePreferences", 0);
        return pref.getString(preferenceToGet,null);
    }

    public static Boolean isLoggedIn(Context context) {
        return getUserId(context) != null ? true : false;
    }

    public static void logout(Context context) {
        SharedPreferences pref = context.getSharedPreferences("GeoAssistancePreferences", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public static Boolean login(Context context, JSONObject parsedUser) {
        try {
            SharedPreferences pref = context.getSharedPreferences("GeoAssistancePreferences", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("user_id", parsedUser.getString("user_id"));
            editor.putString("web_user_id", parsedUser.getString("web_user_id"));
            editor.putString("user_email", parsedUser.getString("user_email"));
            editor.putString("user_name", parsedUser.getString("user_name"));
            editor.commit();
            return true;
        } catch (JSONException e) {
            return false;
        }

    }

}
