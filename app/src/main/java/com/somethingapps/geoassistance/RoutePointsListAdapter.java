package com.somethingapps.geoassistance;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;
import com.somethingapps.geoassistance.RequestManagerDataParser.RoutePoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.LOCATION_SERVICE;


public class RoutePointsListAdapter extends BaseAdapter implements View.OnClickListener, LocationListener, ServerRequest.IServerRequest {
    Context context;
    ArrayList<RoutePoint> routePointsList;
    Location location = null;
    LocationManager locationManager = null;
    RoutePoint selectedRoutePoint = null;

    private static LayoutInflater inflater = null;

    public RoutePointsListAdapter(Context context, ArrayList<RoutePoint> routes) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.routePointsList = routes;
        locationManager = (LocationManager) this.context.getSystemService(LOCATION_SERVICE);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        } catch (SecurityException e) {
            Toast.makeText(context, "Error getting GPS location!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return routePointsList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return routePointsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.route_point_list_row, null);
        }

        TextView description = (TextView) vi.findViewById(R.id.geo_element_description_route_point_row_text);
        TextView duration = (TextView) vi.findViewById(R.id.route_duration_route_point_row_text);
        TextView destination = (TextView) vi.findViewById(R.id.route_destination_route_point_row_text);
        TextView state = (TextView) vi.findViewById(R.id.route_point_state_row_text);

        description.setText(vi.getResources().getString(R.string.description) + ": " + routePointsList.get(position).getGeoElementDescription());
        duration.setText(vi.getResources().getString(R.string.distance) + ": " + routePointsList.get(position).getDistance());
        destination.setText(vi.getResources().getString(R.string.destination) + ": " + routePointsList.get(position).getDestination());

        Button startRouteBtn = (Button) vi.findViewById(R.id.start_repair_btn);
        Button getDirectionsBtn = (Button) vi.findViewById(R.id.get_directions_btn);
        startRouteBtn.setTag(routePointsList.get(position));
        getDirectionsBtn.setTag(routePointsList.get(position));
        startRouteBtn.setOnClickListener(this);
        getDirectionsBtn.setOnClickListener(this);

        Date started = Utilities.convertDate(routePointsList.get(position).getStarted());
        String stateText;

        if (started == null) {
            stateText = vi.getResources().getString(R.string.not_started);

        } else {
            stateText = vi.getResources().getString(R.string.started) + ": " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(started);
            startRouteBtn.setVisibility(View.GONE);
        }

        state.setText(stateText);

        return vi;
    }

    private void startGoogleMapsNavigation(RoutePoint routePoint) {
        String destination = routePoint.getLatitude() + "," + routePoint.getLongitude();
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + destination);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        context.startActivity(mapIntent);
    }

    private void startRepairActivity(RoutePoint routePoint) {
        Intent myIntent = new Intent(context, RepairActivity.class);
        myIntent.putExtra("id", routePoint.getId());
        myIntent.putExtra("geoElementId", routePoint.getGeoElementId());
        context.startActivity(myIntent);
    }

    @Override
    public void onClick(View view) {
        selectedRoutePoint = (RoutePoint) view.getTag();
        switch (view.getId()) {
            case R.id.start_repair_btn:
                showYesNoDialog();
                break;
            case R.id.get_directions_btn:
                startGoogleMapsNavigation(selectedRoutePoint);
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void showYesNoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(context.getResources().getString(R.string.start_new_route_point_repair));
        builder.setMessage(context.getResources().getString(R.string.start_new_route_point_repair_are_you_sure));

        builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                UpdateRoutePointAndStartRepair();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void UpdateRoutePointAndStartRepair() {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ServerRequest serverRequest = new ServerRequest(context);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.UPDATE_ROUTE_POINT_STARTED, context);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("route_point_id", Integer.toString(selectedRoutePoint.getId())));
        serverRequestData.AddQueryString(new QueryData("started", format.format(new Date())));
        serverRequest.callRequest(serverRequestData);
    }

    private void onUpdateRouteDataRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            JSONObject jsonObject = new JSONObject(serverRequestResponse.getResult().toString());
            Boolean result = jsonObject.getBoolean("updateResult");
            if (result) {
                startRepairActivity(selectedRoutePoint);
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.error_updating_route_point), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(context, context.getResources().getString(R.string.error_updating_route_point) + "\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            switch (serverRequestResponse.getServerRequestType().getRequestType()) {
                case UPDATE_ROUTE_POINT_STARTED:
                    onUpdateRouteDataRequestResponse(serverRequestResponse);
                    break;
            }
        } catch (Exception ex) {
            Toast.makeText(context, context.getResources().getString(R.string.unknown_error) + "\n" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
