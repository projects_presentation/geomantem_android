package com.somethingapps.geoassistance;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;
import com.somethingapps.geoassistance.RequestManager.UploadPictureData;
import com.somethingapps.geoassistance.RequestManager.UploadPictureRequest;
import com.somethingapps.geoassistance.RequestManagerDataParser.TypeOfRepair;
import com.somethingapps.geoassistance.RequestManagerDataParser.TypesOfRepairRequestResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RepairActivity extends AppCompatActivity implements View.OnClickListener, CheckBox.OnCheckedChangeListener, ServerRequest.IServerRequest, UploadPictureRequest.IUploadPictureRequest {

    private int routePointId = 0;
    private int geoElementId = 0;

    private LinearLayout images_gallery;
    private EditText observationsTv;
    private ListView typesOfRepairsListView;
    private CheckBox statusCheckBox;
    private CheckBox noRepairCheckBox;

    private Button takePictureBtn;
    private Button sendPicturesBtn;
    private Button finishRepairBtn;

    private int pictureId = 0;

    private static final int TAKE_PICTURE_REQUEST_CODE = 5000;

    private File pictureFile = null;
    private Uri pictureURI = null;

    private static final int MAX_PICTURES_COUNT = 10;

    private boolean insertTypesResult = false,updateRoutePointObservationsResult = false,updateRoutePointEndedResult = false, updateEquipmentStatusResult = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startUI();
        routePointId = getIntent().getIntExtra("id", 0);
        geoElementId = getIntent().getIntExtra("geoElementId", 0);

        FillImagesList();
        FillTypesOfRepairList();
    }

    private void startUI() {
        setContentView(R.layout.repair_activity);
        images_gallery = (LinearLayout) findViewById(R.id.repair_images_gallery);
        typesOfRepairsListView = (ListView) findViewById(R.id.type_of_repairs_list_view);
        observationsTv = (EditText) findViewById(R.id.repair_observations_text);
        statusCheckBox = (CheckBox) findViewById(R.id.repair_final_status_checkbox);
        noRepairCheckBox = (CheckBox) findViewById(R.id.no_repair_final_status_checkbox);
        takePictureBtn = (Button) findViewById(R.id.take_repair_picture_btn);
        sendPicturesBtn = (Button) findViewById(R.id.send_photos_to_server_btn);
        finishRepairBtn = (Button) findViewById(R.id.finish_repair_btn);

        statusCheckBox.setOnCheckedChangeListener(this);
        noRepairCheckBox.setOnCheckedChangeListener(this);

        takePictureBtn.setOnClickListener(this);
        sendPicturesBtn.setOnClickListener(this);
        finishRepairBtn.setOnClickListener(this);

        observationsTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                RepairActivityData repairActivityData = RepairActivitySavedData.getRepairActivityData(routePointId);
                if (repairActivityData != null) {
                    repairActivityData.setObservations(observationsTv.getText().toString());
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    //region Pictures Handling
    private void FillImagesList() {
        RepairActivityData repairActivityData = RepairActivitySavedData.getRepairActivityData(routePointId);
        if (repairActivityData != null) {
            observationsTv.setText(repairActivityData.getObservations());
            for (int i = 0; i < repairActivityData.getImages().size(); ++i) {
                AddImageToGallery(repairActivityData.getThumbnails().get(i));
            }
        }
    }

    private void AddImageToGallery(Bitmap thumbnail) {
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.FIT_START);
        imageView.setId(pictureId++);
        imageView.setPadding(5, 5, 5, 5);
        imageView.setImageBitmap(thumbnail);
        images_gallery.addView(imageView);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.take_repair_picture_btn:
                if (images_gallery.getChildCount() >= MAX_PICTURES_COUNT) {
                    Toast.makeText(this, getResources().getString(R.string.sent_pictures_number, MAX_PICTURES_COUNT, 0), Toast.LENGTH_SHORT).show();
                } else {
                    takePictureFromCamera();
                }
                break;
            case R.id.send_photos_to_server_btn:
                UploadPicturesToServer();
                UploadRepairTypesToServer();
                UpdatedRoutePointObservations();
                UpdateEquipmentRepairStatus();
                break;
            case R.id.finish_repair_btn:
                showYesNoDialog();
                break;
        }
    }

    private void takePictureFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

        pictureFile = new File(dir, routePointId + "" + pictureId + ".jpeg");
        pictureURI = FileProvider.getUriForFile(RepairActivity.this, BuildConfig.APPLICATION_ID + ".provider", pictureFile);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureURI);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, TAKE_PICTURE_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAKE_PICTURE_REQUEST_CODE && resultCode == RESULT_OK) {

            try {
                Bitmap image = MediaStore.Images.Media.getBitmap(getContentResolver(), pictureURI);
                Bitmap thumbnail = ThumbnailUtils.extractThumbnail(image, 200, 300);

                AddImageToGallery(thumbnail);
                RepairActivitySavedData.addActivityData(routePointId, thumbnail, image);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //endregion

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {

        String TAG = "RESULT_DEBUG";

        try {
            switch (serverRequestResponse.getServerRequestType().getRequestType()) {
                case GET_TYPES_OF_REPAIR:
                    onGetTypeOfRepairsRequestResult(serverRequestResponse);
                    break;
                case INSERT_TYPES_OF_REPAIR:
                    Log.d(TAG,"INSERT_TYPES_OF_REPAIR");
                    insertTypesResult = new JSONObject(serverRequestResponse.getResult().toString()).getBoolean("insertResult");
                    break;
                case UPDATE_ROUTE_POINT_OBSERVATIONS:
                    Log.d(TAG,"UPDATE_ROUTE_POINT_OBSERVATIONS");
                    updateRoutePointObservationsResult=new JSONObject(serverRequestResponse.getResult().toString()).getBoolean("updateResult");
                    break;
                case UPDATE_ROUTE_POINT_ENDED:
                    Log.d(TAG,"UPDATE_ROUTE_POINT_ENDED");
                    updateRoutePointEndedResult = new JSONObject(serverRequestResponse.getResult().toString()).getBoolean("updateResult");
                    if(updateRoutePointEndedResult){
                        finish();
                    }
                    break;
                case UPDATE_EQUIPMENT_STATUS:
                    Log.d(TAG,"UPDATE_EQUIPMENT_STATUS");
                    updateEquipmentStatusResult = new JSONObject(serverRequestResponse.getResult().toString()).getBoolean("updateResult");
                    handleAllResults();
                    break;
            }
        } catch (Exception ex) {
            Toast.makeText(this, getResources().getString(R.string.unknown_error) + "\n" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void handleAllResults(){

        new Thread(){
            public void run(){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int counter=5;
                        ProgressDialog progressDialog = new ProgressDialog(RepairActivity.this);
                        progressDialog.setMessage("Teste, teste!");
                        progressDialog.show();
                        while (!ServerRequest.queueIsEmpty()){
                            try {
                                Thread.sleep(1000);
                                if(--counter == 0) break;
                            } catch (Exception ex){
                                Log.d("THREAD_DEBUG", ex.getMessage());
                            }
                        }
                        progressDialog.dismiss();
                        if(insertTypesResult && updateRoutePointObservationsResult && updateEquipmentStatusResult){
                            Toast.makeText(getApplicationContext(), "Todos os dados foram enviados com sucesso!", Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Erro ao enviar todos os dados!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }.start();
    }

    private void FillTypesOfRepairList() {

        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.GET_TYPES_OF_REPAIR, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("web_user_id", LoggedInUser.getWebUserId(this)));
        serverRequest.callRequest(serverRequestData);

    }

    public void onGetTypeOfRepairsRequestResult(ServerRequestResponse serverRequestResponse) {
        try {
            JSONArray jsonArray = new JSONArray(serverRequestResponse.getResult().toString());
            TypesOfRepairRequestResult result = new TypesOfRepairRequestResult(jsonArray);
            typesOfRepairsListView.setAdapter(new TypeOfRepairsListAdapter(this, result.getTypesOfRepairList()));
        } catch (Exception e) {
            Toast.makeText(this, getResources().getString(R.string.error_getting_types_of_repair) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    private void UploadRepairTypesToServer() {
        ArrayList<TypeOfRepair> typeOfRepairsList = new ArrayList<>();
        Adapter adapter = typesOfRepairsListView.getAdapter();
        for (int i = 0; i < adapter.getCount(); ++i) {
            TypeOfRepair typeOfRepair = (TypeOfRepair) adapter.getItem(i);
            if (typeOfRepair.isSelected()) {
                typeOfRepairsList.add(typeOfRepair);
            }
        }
        if (typeOfRepairsList.size() != 0) {

            ServerRequest serverRequest = new ServerRequest(this);
            serverRequest.setOnServerRequestResponse(this);
            ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.INSERT_TYPES_OF_REPAIR, this);
            ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
            serverRequestData.AddQueryString(new QueryData("route_point_id", Integer.toString(routePointId)));
            for (TypeOfRepair typeOfRepair : typeOfRepairsList) {
                serverRequestData.AddQueryString(new QueryData("type_of_repair_id", Integer.toString(typeOfRepair.getId())));
            }
            serverRequest.callRequest(serverRequestData);

        }
    }

    private void UploadPicturesToServer() {

        RepairActivityData repairActivityData = RepairActivitySavedData.getRepairActivityData(routePointId);

        if (repairActivityData != null) {
            UploadPictureData uploadPictureData = new UploadPictureData(routePointId, repairActivityData, this);
            UploadPictureRequest uploadPictureRequest = new UploadPictureRequest();
            uploadPictureRequest.setOnUpdatePictureRequestListener(this);
            uploadPictureRequest.execute(uploadPictureData);
        } else {
            //Toast.makeText(this, "There are no pictures to send!", Toast.LENGTH_SHORT).show();
        }
    }

    private void UpdatedRoutePointObservations() {

        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.UPDATE_ROUTE_POINT_OBSERVATIONS, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("route_point_id", Integer.toString(routePointId)));
        serverRequestData.AddQueryString(new QueryData("observations", observationsTv.getText().toString()));
        serverRequest.callRequest(serverRequestData);
    }

    private void UpdateEquipmentRepairStatus() {

        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.UPDATE_EQUIPMENT_STATUS, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);

        if (statusCheckBox.isChecked()) {
            serverRequestData.AddQueryString(new QueryData("repair_type_id", Integer.toString(1)));
            serverRequestData.AddQueryString(new QueryData("geoElementId", Integer.toString(geoElementId)));
            serverRequest.callRequest(serverRequestData);
        } else if (noRepairCheckBox.isChecked()) {
            serverRequestData.AddQueryString(new QueryData("repair_type_id", Integer.toString(3)));
            serverRequestData.AddQueryString(new QueryData("geoElementId", Integer.toString(geoElementId)));
            serverRequest.callRequest(serverRequestData);
        }

    }

    @Override
    public void onUploadPictureRequestResponse(Boolean result) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.send_repair_data_to_server));
        if (result) {
            images_gallery.removeAllViews();
            RepairActivitySavedData.removeAllById(routePointId);
            alert.setMessage(getResources().getString(R.string.data_upload_success));
            //Toast.makeText(this, getResources().getString(R.string.data_upload_success), Toast.LENGTH_SHORT).show();
        } else {
            alert.setMessage(getResources().getString(R.string.data_upload_unsuccessfull));
        }
        alert.setPositiveButton("OK", null);
        alert.show();
    }

    private void showYesNoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getResources().getString(R.string.end_repair));
        builder.setMessage(getResources().getString(R.string.end_repair_are_you_sure));

        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                UploadPicturesToServer();
                UploadRepairTypesToServer();
                UpdatedRoutePointObservations();
                UpdateEquipmentRepairStatus();
                finishRepair();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void finishRepair() {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.UPDATE_ROUTE_POINT_ENDED, this);
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("route_point_id", Integer.toString(routePointId)));
        serverRequestData.AddQueryString(new QueryData("ended", format.format(new Date())));
        serverRequest.callRequest(serverRequestData);

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        CheckBox checkBox = (CheckBox) compoundButton;
        if (checkBox.equals(statusCheckBox)) {
            if (statusCheckBox.isChecked()) {
                noRepairCheckBox.setChecked(false);
            }
        } else if (checkBox.equals(noRepairCheckBox)) {
            if (noRepairCheckBox.isChecked()) {
                statusCheckBox.setChecked(false);
            }
        }
    }

}
