package com.somethingapps.geoassistance.RequestManagerDataParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jose on 27/06/2017.
 */

public class EquipmentLocationRequestResults {
    private ArrayList<EquipmentLocation> equipmentLocationsList;

    public EquipmentLocationRequestResults(JSONArray results){
        parseResult(results);
    }

    private void parseResult(JSONArray results){
        equipmentLocationsList = new ArrayList<EquipmentLocation>();

        for(int i=0; i< results.length();++i){
            try {
                JSONObject routeJsonObject = results.getJSONObject(i);
                int id = routeJsonObject.getInt("id");
                String location = routeJsonObject.getString("description");
                EquipmentLocation equipmentLocation= new EquipmentLocation(id, location);
                equipmentLocationsList.add(equipmentLocation);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<EquipmentLocation> getEquipmentLocationsList(){
        return equipmentLocationsList;
    }
}
