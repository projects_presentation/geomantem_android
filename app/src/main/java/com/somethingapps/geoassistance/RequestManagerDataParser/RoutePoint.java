package com.somethingapps.geoassistance.RequestManagerDataParser;

public class RoutePoint {
    private int id;
    private int routeId;
    private int geoElementId;
    private String geoElementDescription;
    private Double latitude;
    private Double longitude;
    private String distance;
    private String destination;
    private String started;

    public RoutePoint(int id, int routeId, int geoElementId, String geoElementDescription, Double latitude, Double longitude, String distance, String destination, String started){
        this.id = id;
        this.routeId = routeId;
        this.geoElementId = geoElementId;
        this.geoElementDescription = geoElementDescription;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.destination = destination;
        this.started = started;
    }

    public int getId(){
        return id;
    }

    public int getRouteId() {
        return routeId;
    }

    public int getGeoElementId() {
        return geoElementId;
    }

    public String getGeoElementDescription() {
        return geoElementDescription;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getDistance(){
        return distance;
    }

    public String getDestination(){
        return destination;
    }

    public String getStarted(){
        return started;
    }
}
