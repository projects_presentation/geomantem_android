package com.somethingapps.geoassistance.RequestManagerDataParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TypesOfRepairRequestResult {

    private ArrayList<TypeOfRepair> typesOfRepairList = new ArrayList<>();

    public TypesOfRepairRequestResult(JSONArray results){
        parseResult(results);
    }

    private void parseResult(JSONArray results){
        typesOfRepairList = new ArrayList<>();
        for(int i=0; i< results.length();++i){
            try {
                JSONObject routeJsonObject = results.getJSONObject(i);
                int id = routeJsonObject.getInt("id");
                String description = routeJsonObject.getString("description");
                TypeOfRepair typeOfRepair = new TypeOfRepair(id,description);
                typesOfRepairList.add(typeOfRepair);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<TypeOfRepair> getTypesOfRepairList(){
        return typesOfRepairList;
    }
}
