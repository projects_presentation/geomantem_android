package com.somethingapps.geoassistance.RequestManagerDataParser;

public class TypeOfRepair {
    private int id;
    private boolean isSelected = false;
    private String description;

    public TypeOfRepair(int id, String description){
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString(){
        return description;
    }
}
