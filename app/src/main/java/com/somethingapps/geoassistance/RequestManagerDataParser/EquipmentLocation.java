package com.somethingapps.geoassistance.RequestManagerDataParser;

public class EquipmentLocation {
    private int id;
    private String location;

    public EquipmentLocation(int id, String location){
        this.id = id;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString(){
        return this.location;
    }
}
