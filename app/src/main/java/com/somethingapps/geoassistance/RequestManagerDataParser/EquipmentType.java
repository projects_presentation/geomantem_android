package com.somethingapps.geoassistance.RequestManagerDataParser;

public class EquipmentType {
    private int id;
    private String type;

    public EquipmentType(int id, String type){
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString(){
        return this.type;
    }
}
