package com.somethingapps.geoassistance.RequestManagerDataParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RoutePointsRequestResult {

    private ArrayList<RoutePoint> routePointsList;

    public RoutePointsRequestResult(JSONArray results){
        parseResult(results);
    }

    private void parseResult(JSONArray results){
        routePointsList = new ArrayList<RoutePoint>();
        for(int i=0; i< results.length();++i){
            try {
                JSONObject routeJsonObject = results.getJSONObject(i);
                int id = routeJsonObject.getInt("id");
                int routeId = routeJsonObject.getInt("route_id");
                int geoElementId = routeJsonObject.getInt("geo_element_id");
                String geoElementDescription = routeJsonObject.getString("geo_element_description");
                Double latitude = routeJsonObject.getDouble("latitude");
                Double longitude = routeJsonObject.getDouble("longitude");
                String distance = routeJsonObject.getString("distance");
                String destination = routeJsonObject.getString("destination").split(",")[0];
                String started = routeJsonObject.getString("started");
                RoutePoint routePoint = new RoutePoint(id,routeId,geoElementId,geoElementDescription,latitude,longitude,distance,destination,started);
                routePointsList.add(routePoint);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<RoutePoint> getRoutePointsList(){
        return routePointsList;
    }


}

