package com.somethingapps.geoassistance.RequestManagerDataParser;

public class Route {
    private String serialNumber = "";
    private int id = 0;
    private int webUserId = 0;
    private int subTypeId = 0;
    private int typeId = 0;
    private String location = "";
    private int estimatedDuration = 0;
    private int estimatedDistance = 0;
    private String subTypeDescription = "";
    private String started;


    public Route(String serialNumber, int id, int typeId, int subTypeId, int webUserId, int estimatedDuration, int estimatedDistance, String location, String subTypeDescription, String started){
        this.serialNumber = serialNumber;
        this.id = id;
        this.typeId = typeId;
        this.subTypeId = subTypeId;
        this.webUserId = webUserId;
        this.estimatedDuration = estimatedDuration;
        this.estimatedDistance = estimatedDistance;
        this.location = location;
        this.subTypeDescription = subTypeDescription;
        this.started = started;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public int getId() {
        return id;
    }

    public int getTypeId() {
        return typeId;
    }

    public int getSubTypeId() {
        return subTypeId;
    }

    public int getWebUserId() {
        return webUserId;
    }

    public int getEstimatedDuration() {
        return estimatedDuration;
    }

    public int getEstimatedDistance() {
        return estimatedDistance;
    }

    public String getLocation() {
        return location;
    }

    public String getSubTypeDescription() {
        return subTypeDescription;
    }

    public String getStarted(){
        return started;
    }

}
