package com.somethingapps.geoassistance.RequestManagerDataParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jose on 27/06/2017.
 */

public class EquipmentTypeRequestResults {
    private ArrayList<EquipmentType> equipmentTypesList;

    public EquipmentTypeRequestResults(JSONArray results){
        parseResult(results);
    }

    private void parseResult(JSONArray results){
        equipmentTypesList = new ArrayList<EquipmentType>();

        for(int i=0; i< results.length();++i){
            try {
                JSONObject routeJsonObject = results.getJSONObject(i);
                int id = routeJsonObject.getInt("id");
                String location = routeJsonObject.getString("description");
                EquipmentType equipmentType= new EquipmentType(id, location);
                equipmentTypesList.add(equipmentType);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<EquipmentType> getEquipmentTypesList(){
        return equipmentTypesList;
    }
}

