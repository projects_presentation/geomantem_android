package com.somethingapps.geoassistance.RequestManagerDataParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RoutesRequestResult {

    private ArrayList<Route> routes;

    public RoutesRequestResult(JSONArray results){
        parseResult(results);
    }

    private void parseResult(JSONArray results){
        routes = new ArrayList<Route>();
        for(int i=0; i< results.length();++i){
            try {
                JSONObject routeJsonObject = results.getJSONObject(i);
                String serialNumber = routeJsonObject.getString("serial_number");
                int id = routeJsonObject.getInt("id");
                int typeId = routeJsonObject.getInt("type_id");
                int subTypeId = routeJsonObject.getInt("sub_type_id");
                int webUserId = routeJsonObject.getInt("web_user_id");
                int estimatedDuration = routeJsonObject.getInt("estimated_duration");
                int estimatedDistance = routeJsonObject.getInt("estimated_distance");
                String location = routeJsonObject.getString("location");
                String subTypeDescription = routeJsonObject.getString("sub_type_description");
                String started = routeJsonObject.getString("started");
                Route route = new Route(serialNumber,id,typeId,subTypeId,webUserId,estimatedDuration,estimatedDistance,location,subTypeDescription,started);
                routes.add(route);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Route> getRoutes(){
        return routes;
    }


}
