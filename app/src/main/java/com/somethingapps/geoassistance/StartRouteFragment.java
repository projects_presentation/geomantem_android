package com.somethingapps.geoassistance;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.somethingapps.geoassistance.RequestManager.QueryData;
import com.somethingapps.geoassistance.RequestManager.ServerRequest;
import com.somethingapps.geoassistance.RequestManager.ServerRequestData;
import com.somethingapps.geoassistance.RequestManager.ServerRequestResponse;
import com.somethingapps.geoassistance.RequestManager.ServerRequestType;
import com.somethingapps.geoassistance.RequestManagerDataParser.Route;
import com.somethingapps.geoassistance.RequestManagerDataParser.RoutesRequestResult;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class StartRouteFragment extends Fragment implements ServerRequest.IServerRequest, AdapterView.OnItemClickListener{

    ListView listview;

    private OnFragmentInteractionListener mListener;

    public StartRouteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_route_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        startUI();
        FillRoutesList();
    }

    private void startUI(){
        TextView loginAppName = (TextView)getView().findViewById(R.id.start_new_route_title);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/main.ttf");
        loginAppName.setTypeface(custom_font);
    }

    private void FillRoutesList(){

        ServerRequest serverRequest = new ServerRequest(getContext());
        serverRequest.setOnServerRequestResponse(this);
        ServerRequestType serverRequestType = new ServerRequestType(ServerRequestType.RequestType.GET_ROUTES,getContext());
        ServerRequestData serverRequestData = new ServerRequestData(serverRequestType);
        serverRequestData.AddQueryString(new QueryData("web_user_id",LoggedInUser.getWebUserId(getContext())));
        serverRequestData.AddQueryString(new QueryData("user_id", LoggedInUser.getUserId(getContext())));
        serverRequest.callRequest(serverRequestData);
    }

    @Override
    public void onServerRequestResponse(ServerRequestResponse serverRequestResponse) {
        try {
            switch (serverRequestResponse.getServerRequestType().getRequestType()){
                case GET_ROUTES:
                    onGetAllRoutesRequestResult(serverRequestResponse);
                    break;
            }
        } catch (Exception ex){
            Toast.makeText(getContext(), getResources().getString(R.string.unknown_error) + "\n" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onGetAllRoutesRequestResult(ServerRequestResponse serverRequestResponse) {

        try {
            JSONArray jsonArray = new JSONArray(serverRequestResponse.getResult().toString());
            RoutesRequestResult routesRequestResult = new RoutesRequestResult(jsonArray);
            listview = (ListView) getView().findViewById(R.id.start_routes_list);
            listview.setAdapter(new StartRoutesListAdapter(getContext(), (ArrayList<Route>)routesRequestResult.getRoutes()));
            listview.setItemsCanFocus(false);
            listview.setOnItemClickListener(this);
            if(routesRequestResult.getRoutes().isEmpty()){
                Toast.makeText(getContext(), getResources().getString(R.string.no_route_available), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), getResources().getString(R.string.error_getting_routes) + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        if(listview != null){
            listview.setAdapter(null);
            FillRoutesList();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Route route = (Route)adapterView.getItemAtPosition(i);
        if(Utilities.convertDate(route.getStarted()) != null){
            Intent myIntent = new Intent(getContext(), RoutePointsActivity.class);
            myIntent.putExtra("routeId", route.getId());
            myIntent.putExtra("typeId",route.getTypeId());
            myIntent.putExtra("subTypeId",route.getSubTypeId());
            getContext().startActivity(myIntent);
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
