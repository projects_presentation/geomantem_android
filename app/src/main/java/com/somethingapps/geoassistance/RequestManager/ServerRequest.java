package com.somethingapps.geoassistance.RequestManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.somethingapps.geoassistance.CustomSettings;
import com.somethingapps.geoassistance.Utilities;

public class ServerRequest implements Response.Listener, Response.ErrorListener{

    private ProgressDialog progressDialog;
    private Context context;

    private ServerRequestData serverRequestData = null;
    private IServerRequest iServerRequest;

    private static int queueBufferLength = 0;


    public interface IServerRequest{
        void onServerRequestResponse(ServerRequestResponse serverRequestResponse);
    }

    public void setOnServerRequestResponse(IServerRequest iServerRequest){
        this.iServerRequest = iServerRequest;
    }

    public ServerRequest(Context context){
        this.context = context;
        this.iServerRequest = null;
    }

    public void callRequest(ServerRequestData serverRequestData){
        this.serverRequestData = serverRequestData;
        ShowProgressBar();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url;
        if(CustomSettings.getServerPort() == 80){
            url = CustomSettings.getServerAddress();
        }
        else {
            url = CustomSettings.getServerAddress() +":"+ CustomSettings.getServerPort() + "/";
        }

        url+=serverRequestData.getServerRequestType().getRequestEndpoint();
        int index = 0;
        for (QueryData queryData:serverRequestData.getQueryDataList()
                ) {
            url+=(index++ == 0 ? "?":"&") + queryData.toString();
        }
        StringRequest stringRequest = new StringRequest(serverRequestData.getServerRequestType().getRequestMethod(),url,this,this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
        queueBufferLength++;

    }

    @Override
    public void onResponse(Object response) {
        progressDialog.dismiss();
        ServerRequestResponse serverRequestResponse= new ServerRequestResponse(serverRequestData.getServerRequestType(),response);
        iServerRequest.onServerRequestResponse(serverRequestResponse);
        Utilities.logMessage("Response from server: " + response);
        queueBufferLength--;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressDialog.dismiss();
        Toast.makeText(context, serverRequestData.getServerRequestType().getErrorString() +"\r\n"+ error.getMessage(), Toast.LENGTH_LONG).show();
        iServerRequest.onServerRequestResponse(null);
        error.printStackTrace();
        queueBufferLength--;
    }

    public static boolean queueIsEmpty(){
        return queueBufferLength == 0;
    }

    private void ShowProgressBar(){
        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(serverRequestData.getServerRequestType().getLoadingString());
                progressDialog.show();
            }
        });
    }
}
