package com.somethingapps.geoassistance.RequestManager;

import java.util.ArrayList;

public class ServerRequestData {

    private ServerRequestType serverRequestType;
    private ArrayList<QueryData> queryDataList = new ArrayList<>();

    public ServerRequestData(ServerRequestType serverRequestType){
        this.serverRequestType = serverRequestType;
    }

    public void AddQueryString(QueryData queryData){
        queryDataList.add(queryData);
    }

    public ServerRequestType getServerRequestType() {
        return serverRequestType;
    }

    public ArrayList<QueryData> getQueryDataList() {
        return queryDataList;
    }

}
