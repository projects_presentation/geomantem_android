package com.somethingapps.geoassistance.RequestManager;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.somethingapps.geoassistance.CustomSettings;
import com.somethingapps.geoassistance.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;

public class UploadPictureRequest extends AsyncTask<UploadPictureData, Void, Boolean> {

    private IUploadPictureRequest iUploadPictureRequest;

    public interface IUploadPictureRequest {
        void onUploadPictureRequestResponse(Boolean result);
    }

    public void setOnUpdatePictureRequestListener(IUploadPictureRequest iUploadPictureRequest) {
        this.iUploadPictureRequest = iUploadPictureRequest;
    }

    public UploadPictureRequest() {
        this.iUploadPictureRequest = null;
    }

    private ProgressDialog progressDialog;
    private Context context;

    protected Boolean doInBackground(UploadPictureData... uploadPicturesData) {
        context = uploadPicturesData[0].getContext();
        int routePointId = uploadPicturesData[0].getRoutePointId();
        String observations = null;
        try {
            observations = URLEncoder.encode(uploadPicturesData[0].getRepairActivityData().getObservations(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ShowProgressBar();
        String urlRequest = CustomSettings.getServerAddress() + ":" + CustomSettings.getServerPort() + "/" + ServerRequestType.UPLOAD_PICTURE_ENDPOINT + "?route_point_id=" + routePointId + "&observations=" + observations;
        for (int i = 0; i < uploadPicturesData[0].getRepairActivityData().getImages().size(); ++i) {
            try {
                MultipartUtility multipartUtility = new MultipartUtility(urlRequest);
                multipartUtility.addFilePart("uploadedfile", uploadPicturesData[0].getRepairActivityData().getImages().get(i));
                multipartUtility.finish();
            } catch (Exception ex) {
                return false;
            }
        }
        return true;
    }

    private void ShowProgressBar() {
        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(context.getResources().getString(R.string.uploading_images));
                progressDialog.show();
            }
        });
    }

    protected void onPostExecute(Boolean result) {
        try {
            progressDialog.dismiss();
        }catch (Exception ex){

        }
        iUploadPictureRequest.onUploadPictureRequestResponse(result);
    }

}