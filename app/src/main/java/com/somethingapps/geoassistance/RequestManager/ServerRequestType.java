package com.somethingapps.geoassistance.RequestManager;


import android.content.Context;

import com.android.volley.Request;
import com.somethingapps.geoassistance.R;


public class ServerRequestType {
    public enum RequestType{
        UNKNOWN,
        LOGIN,
        LOGOUT,
        GET_ROUTES,
        UPDATE_ROUTE_STARTED,
        UPDATE_ROUTE_ENDED,
        GET_ROUTE_POINTS,
        UPDATE_ROUTE_POINT_STARTED,
        UPDATE_ROUTE_POINT_ENDED,
        UPDATE_ROUTE_POINT_OBSERVATIONS,
        UPDATE_ROUTE_POINT,
        GET_EQUIPMENTS_LOCATIONS,
        GET_EQUIPMENT_TYPES,
        INSERT_EQUIPMENT,
        UPDATE_EQUIPMENT_STATUS,
        INSERT_EQUIPMENT_IN_ROUTE,
        GET_TYPES_OF_REPAIR,
        INSERT_TYPES_OF_REPAIR
    }

    private Context context;
    private RequestType requestType = RequestType.UNKNOWN;
    private int requestMethod;
    private String requestEndpoint;
    private String loadingString, errorString;

    public static final String LOGIN_ENDPOINT = "mobile_users/login";
    public static final String LOGOUT_ENDPOINT = "mobile_users/logout";
    public static final String GET_ROUTES_ENDPOINT = "routes/get_routes_to_start";
    public static final String UPDATE_ROUTE_STARTED_ENDPOINT = "routes/update_started";
    public static final String UPDATE_ROUTE_ENDED_ENDPOINT = "routes/update_ended";
    public static final String GET_ROUTE_POINTS_ENDPOINT = "route_points/get_mobile";
    public static final String UPDATE_ROUTE_POINT_STARTED_ENDPOINT = "route_points/update_started";
    public static final String UPDATE_ROUTE_POINT_ENDED_ENDPOINT = "route_points/update_ended";
    public static final String UPDATE_ROUTE_POINT_OBSERVATIONS_ENDPOINT = "route_points/update_observations";
    public static final String UPDATE_ROUTE_POINT_ENDPOINT = "route_points/update_by_column";
    public static final String UPLOAD_PICTURE_ENDPOINT = "route_points/upload_picture";
    public static final String GET_EQUIPMENT_LOCATIONS_ENDPOINT = "types/get_mobile";
    public static final String GET_EQUIPMENT_TYPES_ENDPOINT = "sub_types/get_mobile";
    public static final String INSERT_EQUIPMENT_ENDPOINT = "geo_elements/insert_mobile";
    public static final String UPDATE_EQUIPMENT_STATUS_ENDPOINT = "geo_elements/update_status";
    public static final String INSERT_EQUIPMENT_IN_ROUTE_ENDPOINT = "route_points/insert_mobile";
    public static final String GET_TYPES_OF_REPAIR_ENDPOINT = "type_of_repairs/get_mobile";
    public static final String INSERT_TYPES_OF_REPAIR_ENDPOINT = "type_of_repairs/insert_types_of_repair";

    public ServerRequestType(RequestType requestType, Context context){
        this.context = context;
        //Utilities.logMessage("ServerRequestType: " + this.getRequestType().name() +"-"+ requestType.name());
        this.requestType=requestType;
        switch (requestType){
            case LOGIN:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.logging_in);
                errorString = getStringFromResources(R.string.error_logging_in);
                requestEndpoint = LOGIN_ENDPOINT;
                break;
            case LOGOUT:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.logging_out);
                errorString = getStringFromResources(R.string.error_logging_out);
                requestEndpoint = LOGOUT_ENDPOINT;
                break;
            case GET_ROUTES:
                requestMethod = Request.Method.GET;
                loadingString = getStringFromResources(R.string.getting_routes);
                errorString = getStringFromResources(R.string.error_getting_routes);
                requestEndpoint = GET_ROUTES_ENDPOINT;
                break;
            case UPDATE_ROUTE_STARTED:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.updating_route);
                errorString = getStringFromResources(R.string.error_updating_route);
                requestEndpoint = UPDATE_ROUTE_STARTED_ENDPOINT;
                break;
            case UPDATE_ROUTE_ENDED:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.updating_route);
                errorString = getStringFromResources(R.string.error_updating_route);
                requestEndpoint = UPDATE_ROUTE_ENDED_ENDPOINT;
                break;
            case GET_ROUTE_POINTS:
                requestMethod = Request.Method.GET;
                loadingString = getStringFromResources(R.string.getting_route_points);
                errorString = getStringFromResources(R.string.error_getting_route_points);
                requestEndpoint = GET_ROUTE_POINTS_ENDPOINT;
                break;
            case UPDATE_ROUTE_POINT_STARTED:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.updating_route_point);
                errorString = getStringFromResources(R.string.error_updating_route_point);
                requestEndpoint = UPDATE_ROUTE_POINT_STARTED_ENDPOINT;
                break;
            case UPDATE_ROUTE_POINT_ENDED:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.updating_route_point);
                errorString = getStringFromResources(R.string.error_updating_route_point);
                requestEndpoint = UPDATE_ROUTE_POINT_ENDED_ENDPOINT;
                break;
            case UPDATE_ROUTE_POINT_OBSERVATIONS:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.updating_route_point);
                errorString = getStringFromResources(R.string.error_updating_route_point);
                requestEndpoint = UPDATE_ROUTE_POINT_OBSERVATIONS_ENDPOINT;
                break;
            case UPDATE_ROUTE_POINT:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.updating_route_point);
                errorString = getStringFromResources(R.string.error_updating_route_point);
                requestEndpoint = UPDATE_ROUTE_POINT_ENDPOINT;
                break;
            case GET_EQUIPMENTS_LOCATIONS:
                requestMethod = Request.Method.GET;
                loadingString = getStringFromResources(R.string.getting_equipment_locations);
                errorString = getStringFromResources(R.string.error_getting_equipment_locations);
                requestEndpoint = GET_EQUIPMENT_LOCATIONS_ENDPOINT;
                break;
            case GET_EQUIPMENT_TYPES:
                requestMethod = Request.Method.GET;
                loadingString = getStringFromResources(R.string.getting_equipment_types);
                errorString = getStringFromResources(R.string.error_getting_equipment_types);
                requestEndpoint = GET_EQUIPMENT_TYPES_ENDPOINT;
                break;
            case INSERT_EQUIPMENT:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.inserting_equipment);
                errorString = getStringFromResources(R.string.error_inserting_equipment);
                requestEndpoint = INSERT_EQUIPMENT_ENDPOINT;
                break;
            case UPDATE_EQUIPMENT_STATUS:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.updating_equipment_status);
                errorString = getStringFromResources(R.string.error_updating_equipment_status);
                requestEndpoint = UPDATE_EQUIPMENT_STATUS_ENDPOINT;
                break;
            case INSERT_EQUIPMENT_IN_ROUTE:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.inserting_equipment_into_route);
                errorString = getStringFromResources(R.string.error_inserting_equipment);
                requestEndpoint = INSERT_EQUIPMENT_IN_ROUTE_ENDPOINT;
                break;
            case GET_TYPES_OF_REPAIR:
                requestMethod = Request.Method.GET;
                loadingString = getStringFromResources(R.string.getting_types_of_repair);
                errorString = getStringFromResources(R.string.error_getting_types_of_repair);
                requestEndpoint = GET_TYPES_OF_REPAIR_ENDPOINT;
                break;
            case INSERT_TYPES_OF_REPAIR:
                requestMethod = Request.Method.POST;
                loadingString = getStringFromResources(R.string.inserting_types_of_repair);
                errorString = getStringFromResources(R.string.error_inserting_types_of_repair);
                requestEndpoint = INSERT_TYPES_OF_REPAIR_ENDPOINT;
                break;

        }
    }

    private String getStringFromResources(int stringId){
        return context.getResources().getString(stringId);
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public int getRequestMethod() {
        return requestMethod;
    }

    public String getRequestEndpoint() {
        return requestEndpoint;
    }

    public String getLoadingString() {
        return loadingString;
    }

    public String getErrorString() {
        return errorString;
    }
}
