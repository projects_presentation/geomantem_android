package com.somethingapps.geoassistance.RequestManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by jose on 02/07/2017.
 */

public class QueryData {
    private String parameter,value;

    public QueryData(String parameter, String value){
        this.parameter = parameter;
        this.value = value;
    }

    @Override
    public String toString(){
        try {
            return parameter+"="+URLEncoder.encode(value,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return parameter+"="+value;
    }
}
