package com.somethingapps.geoassistance.RequestManager;

import android.content.Context;

import com.somethingapps.geoassistance.RepairActivityData;

public class UploadPictureData {

    private int routePointId;
    private RepairActivityData repairActivityData;
    private Context context;

    public UploadPictureData(int routePointId, RepairActivityData repairActivityData, Context context){
        this.routePointId = routePointId;
        this.repairActivityData = repairActivityData;
        this.context = context;
    }

    public int getRoutePointId() {
        return routePointId;
    }

    public RepairActivityData getRepairActivityData(){
        return  repairActivityData;
    }

    public Context getContext(){
        return context;
    }
}