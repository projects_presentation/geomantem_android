package com.somethingapps.geoassistance.RequestManager;

public class ServerRequestResponse {

    private ServerRequestType serverRequestType;
    private Object result;

    public ServerRequestResponse(ServerRequestType serverRequestType,Object result){
        this.serverRequestType = serverRequestType;
        this.result = result;

    }

    public ServerRequestType getServerRequestType() {
        return serverRequestType;
    }

    public Object getResult() {
        return result;
    }
}
